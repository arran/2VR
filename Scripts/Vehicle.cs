using Godot;
using System;

public class Vehicle : VehicleBody
{

	[Export]
	public float MaxEngineForce = 250.0f;

	[Export]
	public float MaxBrakeForce = 10.0f;

	[Export]
	public float MaxSteerAngleDegrees = 32.0f;


	[Export]
	public float SteeringResposiveness = 5.0f;

	private float maxSteerAngle
	{ 
		get
		{
			return Mathf.Deg2Rad(MaxSteerAngleDegrees);
		}
	}

	public override void _PhysicsProcess(float delta)
	{
		float throttle = Input.IsActionPressed("accelerate") ? 1.0f : 0.0f;
		float reverse = Input.IsActionPressed("reverse") ? 1.0f : 0.0f;
		float brake = Input.IsActionPressed("brake") ? 1.0f : 0.0f;

		float engineForce = throttle - reverse;

		if (brake > 0)
			engineForce = 0;

		EngineForce = engineForce * MaxEngineForce;
		Brake = brake * MaxBrakeForce;

		float inputSteering = (
			Input.GetActionStrength("steer_left")
			- Input.GetActionStrength("steer_right"));

		float steerTarget = inputSteering * maxSteerAngle;
		float steerAngle = Steering;

		if (steerTarget < steerAngle)
		{
			steerAngle -= SteeringResposiveness * delta;
			if (steerTarget > steerAngle)
			{
				steerAngle = steerTarget;
			}
		}
		else if (steerTarget > steerAngle)
		{
			steerAngle += SteeringResposiveness * delta;
			if (steerTarget < steerAngle)
			{
				steerAngle = steerTarget;
			}
		}

		Steering = steerAngle;

		base._PhysicsProcess(delta);
	}
}
