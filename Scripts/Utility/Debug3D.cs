using Godot;

public class Debug3D : Control
{

	[Export]
	public NodePath CameraNode;

	[Export]
	public Color DefaultColor;


	private ThirdPersonCamera thirdPersonCamera;


	public override void _Ready()
	{
		base._Ready();
		thirdPersonCamera = GetNode<ThirdPersonCamera>(CameraNode);
		
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		Update();
	}

	public override void _Draw()
	{
		base._Draw();

		// Show movement direction of target
		DrawArrow(
			thirdPersonCamera.UnprojectPosition(thirdPersonCamera.Target.GlobalTransform.origin),
			thirdPersonCamera.UnprojectPosition(thirdPersonCamera.Target.GlobalTransform.origin + thirdPersonCamera.TargetMovementDirection * 10.0f),
			2.0f,
			DefaultColor
		);

	}


	public void DrawArrow(Vector2 start, Vector2 end, float width, Color color)
	{
		DrawLine(start, end, color, width);
		DrawTriangle(end, start.DirectionTo(end), width * 2, color);
	}


	public void DrawTriangle(Vector2 position, Vector2 direction, float size, Color color)
	{
		Vector2 a = position + direction * size;
		Vector2 b = position + direction.Rotated(2 * Mathf.Pi / 3) * size;
		Vector2 c = position + direction.Rotated(4 * Mathf.Pi / 3) * size;
		var points = new Vector2[] {a, b, c};
		DrawPolygon(points, new Color[] {color});
	}
}
