using Godot;
using System;

public static class VRInterfaces
{
	public const string
		OpenVR = "OpenVR",
		OpenXR = "OpenXR";
}

public class Main : Spatial
{
	[Export]
	public NodePath SpectatorViewport;

	[Export]
	public int SpectatorTargetFPS = 60;

	[Export]
	public bool VREnabled = true;

	private float timeSinceLastSpectatorFrame = 0.0f;

	public override void _Ready() {
		UpdateSpectatorWindowSize();
		GetTree().Root.Connect("size_changed", this, "UpdateSpectatorWindowSize");

		bool vrSuccess = false;
		if (VREnabled) vrSuccess = InitializeVR();
		if (vrSuccess) GetTree().Paused = false;

	}

	/// <summary>
	/// Finds the correct VR interface and initializes
	/// </summary>
	/// <returns>If Successful</returns>
	private bool InitializeVR() {
		var vr = ARVRServer.FindInterface(VRInterfaces.OpenVR);
		if (vr != null && vr.Initialize())
		{
			OS.VsyncEnabled = false;
			Engine.TargetFps = 90;
			// Also, the physics FPS in the project settings is also 90 FPS. This makes the physics
			// run at the same frame rate as the display, which makes things look smoother in VR!
			return true;
		}
		return false;
	}


	public override void _Process(float delta) {
		timeSinceLastSpectatorFrame += delta;
		if (timeSinceLastSpectatorFrame > (1.0f / SpectatorTargetFPS)) {
			timeSinceLastSpectatorFrame = 0.0f;
			GetNode<Viewport>(SpectatorViewport).RenderTargetUpdateMode = (Godot.Viewport.UpdateMode) 1;
		}
	}

	private void UpdateSpectatorWindowSize() {
		GetNode<Viewport>(SpectatorViewport).Size = OS.WindowSize;
	}
}
