using System.Linq;
using Godot;
using Godot.Collections;

namespace vr.Scripts.Input
{
	public class VRController : ARVRController
	{
		public bool IsColliding => currentCollidingBody != null;

		public bool IsGripping => IsButtonPressed((int)JoystickList.VrGrip) == 1;

		private PhysicsBody currentCollidingBody;
		
		private Area physicalArea;

		public override void _Ready()
		{
			base._Ready();
			physicalArea = GetChildren().OfType<Area>().First();
		}

		public override void _PhysicsProcess(float delta)
		{
			base._PhysicsProcess(delta);
			Array<PhysicsBody> bodies = new Array<PhysicsBody>(physicalArea.GetOverlappingBodies());
			if (bodies.Count > 0)
			{
				currentCollidingBody = bodies[0];
			}
			else
			{
				currentCollidingBody = null;
			}
		}
	}
}
