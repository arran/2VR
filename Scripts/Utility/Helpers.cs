using Godot;

namespace Utility {
        public static class Helpers {

        /// <summary>
        /// Linearly interpolate between two values by a normalized weight.
        /// </summary>
        public static Vector3 Lerp(Vector3 from, Vector3 to, float weight)
        {
            return new Vector3(
                Mathf.Lerp(from.x, to.x, weight),
                Mathf.Lerp(from.y, to.y, weight),
                Mathf.Lerp(from.z, to.z, weight)
            );
        }

        /// <summary>
        /// Linearly interpolate between two values by a normalized weight.
        /// </summary>
        public static Vector2 Lerp(Vector2 from, Vector2 to, float weight)
        {
            return new Vector2(
                Mathf.Lerp(from.x, to.x, weight),
                Mathf.Lerp(from.y, to.y, weight)
            );
        }
    }
}
