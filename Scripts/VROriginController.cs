using Godot;
using System;
using Valve.VR;
using Object = Godot.Object;


public class VROriginController : ARVROrigin
{
	[Export]
	public NodePath TargetNode;

	[Export]
	public bool SyncTransformWithTarget = false;

	private Spatial target;

	public override void _Ready()
	{
		if (TargetNode != null)	target = GetNode<Spatial>(TargetNode);

		Resource x = GD.Load(
			"res://addons/godot-openvr/OpenVRConfig.gdns");
		if (x != null)
		{
			x.Set("godot_openvr_action_json_path","res://Input/actions.json");
			x.Set("godot_openvr_default_action_set","/actions/godot");
		}

	}

	public override void _PhysicsProcess(float delta)
	{
		if (SyncTransformWithTarget)
		{
			Transform = new Transform(Quat.Identity, target.GlobalTransform.origin);
		}
		base._PhysicsProcess(delta);
	}
}
