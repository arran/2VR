using Godot;
using System;
using Utility;
using static Godot.GD;

public class ThirdPersonCamera : Camera
{

    /// <summary>
    /// Node for the camera to focus on
    /// </summary>
    [Export]    
    public NodePath TargetNode;

    /// <summary>
    /// Mouse to movement Look Sensitivity
    /// </summary>
    [Export]
    public Vector2 LookSensitivity = new Vector2(1.0f, 0.8f);

    /// <summary>
    /// Vertical offset to add to the camera position
    /// </summary>
    [Export]
    public float VerticalOffset = 0f;

    /// <summary>
    /// Desired Distance between the target object and camera
    /// </summary>
    [Export]
    public float DesiredOrbitDistance = 20.0f;

    /// <summary>
    /// Minimum Camera pitch, in degrees.
    /// </summary>
    [Export]
    public float MinPitch = 0.0f;

    /// <summary>
    /// Maximum Camera pitch, in degrees.
    /// </summary>
    [Export]
    public float MaxPitch = 70.0f;

    /// <summary>
    /// Minimum Distance to "Look Ahead" of the target when moving
    /// </summary>
    [Export]
    public float MinVelocityDistance = 1.0f;

    /// <summary>
    /// Maximum Distance to "Look Ahead" of the target when moving
    /// </summary>
    [Export]
    public float MaxVelocityDistance = 10.0f;
    
    /// <summary>
    /// Automattically Align the camera
    /// </summary>
    [Export]
    public bool AutoAlign = false;
    
    /// <summary>
    /// Time before the Camera with auto align with the rotation of the target, in seconds.
    /// </summary>
    [Export]
    public float AutoAlignTime = 1.0f;

    /// <summary>
    /// Responsivness of the AutoAlign adjustment;
    /// </summary>
    [Export]
    public float AutoAlignResponsiveness = 4.5f;

    /// <summary>
    /// Responsive of the orbit translation
    /// </summary>
    [Export]
    public float OrbitResponsiveness = 10.0f;

    /// <summary>
    /// Pitch of the camera when Auto Aligned, in degrees.
    /// </summary>
    public float AutoAlignPitch = 25.0f;


    public Spatial Target;

    public Vector3 TargetMovementDirection;

    private Vector3 lastTargetPosition = new Vector3();
    
    private Vector3 cameraTargetPosition;

    /// <summary>
    /// Target Yaw, in Radians
    /// </summary>
    private float targetYaw;
    
    /// <summary>
    /// Target Pitch, in Radians
    /// </summary>
    private float targetPitch;

    /// <summary>
    /// Time passed since the last input, in Seconds.
    /// </summary>
    private float deltaSinceInput;


    public override void _Ready()
    {
        Target = GetNode<Spatial>(TargetNode);
    
        Input.SetMouseMode(Input.MouseMode.Captured);
        LookAt(Target.GlobalTransform.origin, Vector3.Up);
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (@event is InputEventMouseMotion mouseMotion)
        {
            targetYaw += mouseMotion.Relative.x * ( LookSensitivity.x / 1000);
            targetPitch += mouseMotion.Relative.y * ( LookSensitivity.y / 1000);

            // Clamp the pitch
            targetPitch = Mathf.Clamp(targetPitch, Mathf.Deg2Rad(MinPitch), Mathf.Deg2Rad(MaxPitch));

            // Clamp the targetYaw to 360 Degrees (PI * 2)
            if (targetYaw >= Mathf.Tau)
            {
                targetYaw -= Mathf.Tau;
            }
            else if (targetYaw <= 0)
            {
                targetYaw += Mathf.Tau;
            }

            deltaSinceInput = 0;

        }

        if (@event.IsActionPressed("ui_cancel"))
        {
            GetTree().Quit(0);
        }
    }


    public override void _PhysicsProcess(float delta)
    {
        if (AutoAlign) DeterminteAutoAlignedYaw(delta);

        OrbitTargetPosition(delta);
        LookAtTargetPosition(delta);
    }

    private void DeterminteAutoAlignedYaw(float delta)
    {
        deltaSinceInput += delta;
        if (deltaSinceInput >= AutoAlignTime)
        {
            // Calculate yaw based on the forward direction of the Target;
            var alignedYaw = -Target.Rotation.y - (Mathf.Pi / 2);
            targetYaw = Mathf.LerpAngle(targetYaw, alignedYaw, delta * AutoAlignResponsiveness);
            targetPitch = Mathf.Deg2Rad(AutoAlignPitch);
        }
    }

    private void OrbitTargetPosition(float delta)
    {
        Vector3 desiredUnitPosition;
        desiredUnitPosition.x = Mathf.Cos(targetYaw) * Mathf.Cos(targetPitch);
        desiredUnitPosition.z = Mathf.Sin(targetYaw) * Mathf.Cos(targetPitch);
        desiredUnitPosition.y = Mathf.Sin(targetPitch);

        var targetTranslation = cameraTargetPosition + (desiredUnitPosition * DesiredOrbitDistance);
        var displacement = targetTranslation - Translation;

        Translation += displacement * delta * OrbitResponsiveness;
    }

    private void LookAtTargetPosition(float delta)
    {
        var targetMovement = (Target.Translation - lastTargetPosition) / delta;
        lastTargetPosition = Target.Translation;
        TargetMovementDirection = targetMovement.Normalized();

        Vector3 newCameraAimTarget;

        float targetVelocityMagnitude = targetMovement.LengthSquared();
        if (targetVelocityMagnitude >= MinVelocityDistance)
        {
            var lookAheadDistance = Mathf.Clamp(Mathf.InverseLerp(MinVelocityDistance, MaxVelocityDistance, targetVelocityMagnitude), MinVelocityDistance, MaxVelocityDistance);
            newCameraAimTarget = Target.Translation + ( targetMovement.Normalized() * lookAheadDistance );
        }
        else
        {
            newCameraAimTarget = Target.Translation;
        }

        newCameraAimTarget.y += VerticalOffset;
        cameraTargetPosition = Helpers.Lerp(cameraTargetPosition, newCameraAimTarget, delta * 10.0f);

        LookAt(cameraTargetPosition, Vector3.Up);
    }

}
