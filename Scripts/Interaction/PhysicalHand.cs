﻿using System;
using System.Linq;
using Godot;
using vr.Scripts.Input;

namespace vr.Scripts.Interaction
{
    
    public class RigidBodyHold
    {
        public RigidBody Body;
        public Transform OffsetTransform;

        public RigidBodyHold(RigidBody body, Transform offsetTransform)
        {
            Body = body;
            OffsetTransform = offsetTransform;
        }
    }
    
    public class PhysicalHand : RigidBody
    {
        /// <summary>
        /// reference to VRController to follow
        /// </summary>
        [Export]
        public NodePath VRControllerNode;

        /// <summary>
        /// Indicates if the PhysicalHand is following the Controller 1:1
        /// </summary>
        private bool IsBoundToController => !vrController?.IsColliding ?? true;

        private bool IsGripping => vrController?.IsGripping ?? false;

        private bool IsHoldingObject => currentRigidBodyHold != null;

        private RigidBodyHold currentRigidBodyHold;

        private Spatial targetSpatial;
        private VRController vrController;
        private Area gripArea;
        
        public override void _Ready()
        {
            targetSpatial = GetNode<Spatial>(VRControllerNode);
            vrController = GetNode(VRControllerNode) as VRController;
            gripArea = GetChildren().OfType<Area>().First();
            
            base._Ready();
        }

        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);
            AdjustTransform(delta);

            if (IsGripping)
            {
                attemptToGripItem();
            }

            if (IsHoldingObject)
            {
                MoveItemToGrippedPosition(delta);
            }
            
            if (currentRigidBodyHold != null & !IsGripping)
            {
                releaseItem();
            }
        }
        
        private void attemptToGripItem()
        {
            if (IsHoldingObject) return;

            Godot.Collections.Array areas = gripArea.GetOverlappingAreas();
            foreach (Area area in areas)
            {
                if (area.GetParent() is RigidBody)
                {
                    RigidBody parent = (RigidBody) area.GetParent();
                    HoldItem(parent, area.Transform);
                    return;
                }
            }
			
			
            Godot.Collections.Array bodies = gripArea.GetOverlappingBodies();
            if (bodies.Count < 1) return;
			
            foreach (PhysicsBody body in bodies)
            {           
                if (body is RigidBody)
                {
                    RigidBody rigidBody = (RigidBody) body;
                    HoldItem(rigidBody, determineGrabOffset(rigidBody));
                    return;
                }
            }
        }
        
        private void HoldItem(RigidBody body, Transform offset)
        {
            currentRigidBodyHold = new RigidBodyHold(
                body, offset);
        }

        private void releaseItem()
        {
            currentRigidBodyHold = null;
        }

        private Transform determineGrabOffset(RigidBody body)
        {
            // Store the difference between the hand and the body to preserve orientation
            Basis basisDifference = body.GlobalTransform.basis * GlobalTransform.basis.Inverse();
            return new Transform(basisDifference, Vector3.Zero);
        }


        private Vector3 GetGlobalGripPosition()
        {
            return gripArea.GlobalTransform.origin;
        }
		

        private void MoveItemToGrippedPosition(float delta)
        {
            Quat rotDelta = (GlobalTransform.basis * (currentRigidBodyHold.OffsetTransform.basis * currentRigidBodyHold.Body.GlobalTransform.basis.Inverse())).Quat();
            currentRigidBodyHold.Body.AngularVelocity = rotDelta.Normalized().GetEuler() * (float)( delta * 5000.0 * Math.Acos(rotDelta.w));

            Transform x =  currentRigidBodyHold.Body.GlobalTransform * currentRigidBodyHold.OffsetTransform;
            Vector3 globalGripPosition = x.origin;
            Vector3 displacement = GetGlobalGripPosition() - globalGripPosition;
				
            Vector3 currentForce = currentRigidBodyHold.Body.Mass * currentRigidBodyHold.Body.LinearVelocity;
            Vector3 drawForce = (currentRigidBodyHold.Body.Mass * displacement) * 10000f;
					
            Vector3 appliedForce = (drawForce - currentForce) * delta;
            currentRigidBodyHold.Body.LinearVelocity = appliedForce / currentRigidBodyHold.Body.Mass;
        }

        /// <summary>
        /// Move the PhysicalHand to where to should be
        /// </summary>
        /// <param name="delta"></param>
        private void AdjustTransform(float delta)
        {
            if (IsBoundToController)
            {
                Mode = ModeEnum.Kinematic;
                Transform = targetSpatial.Transform;
            }
            else
            {
                Mode = ModeEnum.Rigid;
                ApplyCentralImpulse(( targetSpatial.Transform.origin - Transform.origin ).Normalized() * delta * 10.0f);
                
                Quat rotDelta = (GlobalTransform.basis * targetSpatial.GlobalTransform.basis.Inverse()).Quat();
                AngularVelocity = rotDelta.Normalized().GetEuler() * (float)( delta * 20.0f * Math.Acos(rotDelta.w));
            }
        }
    }
}